# arg-tech-assessment



## Problem Statement
- [ ] Create ExpressJS server which has three api, Create Book, Get Book and Delete book. 
- [ ] Create API should accpet input like book name, description, number of pages, author name and publisher name. 
- [ ] Get api should return saved book, and should return filtered response in case author name or publisher name is sent in query param. 
- [ ] Delete api should accept single book id and should delete it from database. 
- [ ] All APIs should be protected via JWT token authentication. 
- [ ] You can choose database of your choice, and JWT token you can generate manually, but should include token expiry checks.


## Run the application please follow steps below

- Change the DB url in env as per your environment.

```
npm i
```

- Then start the application

```
npm start
```

- Have attched the postman collection in the repository
