import express from "express";
import BooksController from "../controllers/Books.controller"
import {auth} from "../middlewares/Auth.middleware"

const router = express.Router();


router.post("/", auth.Auth, BooksController.addBook)

router.get("/", auth.Auth, BooksController.fetchBook)

router.delete("/:id", auth.Auth, BooksController.deleteBook)

router.get("/get-auth-token", BooksController.getAuthToken)


export default router;