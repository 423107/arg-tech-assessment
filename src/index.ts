import express from "express";
import dotenv from "dotenv";
dotenv.config();
import cors from "cors";
import { db } from "./db/DB.connect";
db.then(res => {
    // console.log(res)
});
import router from "./routers/Book.router";

// (global as any).TOKEN = "Amol Tribhuwan";

const app = express();
const port =  process.env.PORT || "7443";

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}))


//routes
app.use('/api/v1/books', router);

app.listen(port, () => {
    console.log(`Service successfully run on ${port}`);
    // console.log((global as any).TOKEN);
})

