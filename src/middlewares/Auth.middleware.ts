import jwt, { Secret } from 'jsonwebtoken';
import {Request, Response, NextFunction} from "express"

class Auth{
    commonAuth = async(req : Request, res : Response) => {
        try {
            if(!req.header('authorization')) {
                return {status : 0, response : {
                    Success : false,
                    ResponseMessage : "Please provide valid authorization header with valid bearer token.",
                    Response : null
                }}
             }
            let header : any = req.header('authorization') || "";
            var token : any = header.split(' ') 

            if(token[0] !== 'Bearer'){
                return {status : 0, response : {
                    Success : false,
                    ResponseMessage : "Please provide valid authorization header with valid bearer token.",
                    Response : null
                }};
            }

            token = token[1] 
            if(token == undefined)
                return {status : 0, response : {
                    Success : false,
                    ResponseMessage : "Please provide valid token",
                    Response : null
                }}
            // console.log(user.id);

            // console.log(token)
            // console.log((global as any).TOKEN)
            if((global as any).TOKEN != token){
                return {status : 0, response :{
                    Success : false,
                    ResponseMessage : "Provided bearer token is invalid or inactive",
                    Response : null
                }}
            }

            let access_key : Secret = process.env.ACCESS_KEY || "";
            const decoded : any = jwt.verify(token,  access_key);
            return {status : 1, response : {decoded : decoded}};
        }catch(e : any){
            return {status : 0, response : { Success : false, ResponseMessage: e.message, Response : null }};
        }
    }

    Auth = async (req : Request, res : Response, next : NextFunction) => {
        try {
            
            // (req as CustomRequest).token = decoded;
            let validateJwt = await this.commonAuth(req, res);

            if(validateJwt.status == 0)
                return res.send(validateJwt.response);

            next()
        } catch (e : any) {
            console.log(e)
            return res.status(401).send({ Success : false, ResponseMessage: e.message, Response : null });
        }
    }
}

export const auth = new Auth() 
