import mongoose from "mongoose";
import dotenv from "dotenv";
dotenv.config();

const connectionString : any = process.env.DB_URL;

const options = {
    autoIndex : false,
    maxPoolSize : 10,
    socketTimeoutMS : 45000,
    family : 4
}

export const db = mongoose.connect(connectionString, options)
                    .then(res => {
                        if(res) {
                            console.log(`Database connected successfully.`);
                        }
                    })
                    .catch(err => {
                        console.log("DB connection failure..")
                        console.log(err);
                    })
