import { Request, Response } from "express";
import {BooksValidate, Book} from "../models/Book.model";
import {Types} from "mongoose";
import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";

class BookController {
    addBook = async (req : Request, res : Response) => {
        try {
            const bookObject = {
                bookName : req.body.bookName,
                description : req.body.description,
                numberOfPages : req.body.numberOfPages,
                authorName : req.body.authorName,
                publisherName : req.body.publisherName
            }

            const {error, value} = BooksValidate.validate(bookObject);
            if(error) {
                return res.send({
                    Success : false,
                    ResponseMessage : "Request validation failed.",
                    Response : error.details[0].message
                })
            }else {
                let duplicateBookCheck = await Book.findOne({bookName : req.body.bookName, publisherName : req.body.publisherName, authorName : req.body.authorName})
                if(duplicateBookCheck != null)
                    return res.send({
                        Success : false,
                        ResponseMessage : "Looks like the duplicate record, as the bookName, publisherName, authorName is same.",
                        Response : null
                    })
                let book = await Book.create(bookObject);
                return res.send({
                    Success : true,
                    ResponseMessage : "Book added successfully.",
                    Response : book
                })
            }
        }catch(e) {
            console.log(e);
            return res.send({
                Success : false,
                ResponseMessage : "Interal server error, please try after sometime.",
                Response : null
            })
        }
    }

    fetchBook = async (req : Request, res : Response) => {
        try {
            let query = req.query;
            console.log(typeof query.page)
            let page = (query.page == undefined) ? 1 : (typeof Number(query.page) != 'number') ? 1 : Number(query.page);
            let item = (query.item == undefined) ? 10 : (typeof Number(query.item) != 'number') ? 10 : (Number(query.item) > 100) ? 100 : Number(query.item);
            delete query.page;
            delete query.item;

            // console.log(item * (page-1))
            // console.log(item)
            // console.log(query)

            let book = await Book.find(query).sort({_id : -1}).skip(item * (page-1)).limit(item);

            if(book.length == 0) 
                return res.send({
                    Succcess : true,
                    ResponseMessage : "No Record Found",
                    Response : null
                })
            
            return res.send({
                Succcess : true,
                ResponseMessage : "Record fetched successfully.",
                Response : book
            })
        }catch(e) {
            console.log(e)
            return res.send({
                Success : false,
                ResponseMessage : "Interal server error, please try after sometime.",
                Response : null
            })
        }
    }

    deleteBook = async (req : Request, res : Response) => {
        try {
            let param : any = req.params;
            // console.log(param.id)
            if(Types.ObjectId.isValid(param.id)) {
                if((String)(new Types.ObjectId(param.id)) === param.id){
                    let book = await Book.findByIdAndDelete(new Types.ObjectId(param.id))
                    if(!book) { 
                        return res.send({
                            Success : false,
                            ResponseMessage : 'Book not available.',
                            Response : null
                        })
                    }
                    return res.send({
                        Success : true,
                        ResponseMessage : "Book has been deleted successfully.",
                        Resonse : null
                    })
                }
            }
            return res.send({
                Success : false,
                ResonseMessage : "Please pass valid mongo id.",
                Response : null
            })
        }catch(e) {
            console.log(e)
            return res.send({
                Success : false,
                ResponseMessage : "Interal server error, please try after sometime.",
                Response : null
            })
        }
    }

    getAuthToken = async (req : Request, res : Response) => {
        try {
            // req.body.username

            const access_key : any = process.env.ACCESS_KEY;

            const token = jwt.sign({}, access_key, {
                expiresIn : process.env.JWT_EXPIRES
            });

            (global as any).TOKEN = token;

            return res.send({
                Success : true,
                ResponseMessage : "Auth token generated successfully.",
                Response : {
                    access_token : token
                }
            })
        }catch(e) {
            console.log(e)
            return res.send({
                Success : false,
                ResponseMessage : "Interal server error, please try after sometime.",
                Response : null
            })
        }
    }

}

export default new BookController(); 