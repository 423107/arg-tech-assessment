import mongoose, { Schema, model } from "mongoose";
import Joi from "joi";

export const BooksValidate = Joi.object({
    bookName : Joi.string().required(),
    description : Joi.string().required(),
    numberOfPages : Joi.number().required(),
    authorName : Joi.string().required(),
    publisherName :   Joi.string().required() 
})

interface Books {
    bookName : String,
    description : String,
    numberOfPages : Number,
    authorName : String,
    publisherName : String
}


const BooksSchema = new Schema<Books>({
    bookName : {
        type : String
    },
    description : {
        type : String
    },
    numberOfPages : {
        type : Number
    },
    authorName : {
        type : String
    },
    publisherName : {
        type : String
    }
},
{
    timestamps : true
})


export const Book = model<Books>('Books', BooksSchema);